﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBig : MonoBehaviour
{
    public GameObject Explosion;
    private ScoreManager score;
    public GameObject Score50;
    public AudioSource ExplosionBum;

    public Collider2D coll;
     void update(){
       coll = GetComponent<Collider2D>();
    }
    void Awake()
    {
        score =GameObject.FindGameObjectWithTag("ScoreManager").GetComponent<ScoreManager>();

    }
   
    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag=="Bullet"||other.tag=="ShieldPower"){
                    StartCoroutine(DestroyEnemyBig());

        }else if(other.tag == "Finish2"){
            Destroy(this.gameObject);
        }

    }
    

    IEnumerator DestroyEnemyBig(){
        ExplosionBum.Play();
        //Add 50 Score
        score.AddScore(50);
        //Disable Graphics
        gameObject.GetComponent<Renderer>().enabled = false;
        //Instantiate Explosion
        Instantiate(Score50,transform.position,Quaternion.identity,null); 
        Instantiate(Explosion,transform.position,Quaternion.identity,null);           
        yield return new WaitForSeconds(0.01f);
        Destroy(this.gameObject);
    }
    
}
