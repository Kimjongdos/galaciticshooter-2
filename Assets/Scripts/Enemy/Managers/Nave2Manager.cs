﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nave2Manager : MonoBehaviour
{
    public float CadenciaNave2;
    private float currentTime = 0;
    
    public GameObject Nave2;

    // Update is called once per frame
    void Update()
    {
        currentTime+= Time.deltaTime;
        if(currentTime>CadenciaNave2){
        Instantiate(Nave2,new Vector3(Random.Range(-8.8f,8.8f),6.3f,-1),Quaternion.identity,null);
        currentTime=0;
        }
    }
}
