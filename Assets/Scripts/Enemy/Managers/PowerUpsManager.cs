﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpsManager : MonoBehaviour
{
   
   public GameObject[] PowerUps;
   
   public float Times = 2f;
   private float CurrentTime = 0f;
   
    void SpawnPowerUps(){
    Instantiate(PowerUps[Random.Range(0,5)],transform.position =new Vector3(Random.Range(-7.8f,7.8f),7,0),Quaternion.identity,null);

    }
    
    
    void Update()
    {
        CurrentTime+=Time.deltaTime;
        if (CurrentTime>=Times){
            SpawnPowerUps();
            CurrentTime=0;
            

        }
        
    }
    
}