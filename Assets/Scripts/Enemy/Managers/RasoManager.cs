﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RasoManager : MonoBehaviour
{
   public GameObject[] Raso;
   public int NumberOfTheGameObjects;
   private float timer = 0;
   public float TimeToSpawn; 
   private int RandomEnemy; 

    void start(){
        timer=0;
        NumberOfTheGameObjects++;
               
    }
    void Update()
    {
        RandomEnemy = Random.Range(0,NumberOfTheGameObjects);
        timer+=Time.deltaTime;
        if(timer>TimeToSpawn){
            Instantiate(Raso[RandomEnemy],new Vector3(Random.Range(-8.5f,8.5f),transform.position.y,0),Quaternion.identity,null);
            timer= 0;
        }
    }
}
