﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorretaManager : MonoBehaviour
{
   public GameObject Torreta;
   
   private float timer = 0;
   public float TimeToSpawn; 
   

    void start(){
        timer=0;
       
               
    }
    void Update()
    {
        
        timer+=Time.deltaTime;
        if(timer>TimeToSpawn){
            Instantiate(Torreta,new Vector3(Random.Range(-8.5f,8.5f),transform.position.y,0),Quaternion.identity,null);
            timer= 0;
        }
    }
}
