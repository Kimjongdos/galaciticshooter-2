﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PulpoManager : MonoBehaviour
{
   public float Spawn;
   public GameObject Pulpo;
   public float timeBetweenWaves = 5f;
   private float countDown = 2f;
   private int waves = 0;

    // Update is called once per frame
    
    void SpawnPulpo(){
    Instantiate(Pulpo,transform.position =new Vector3(Random.Range(-7.8f,7.8f),7,0),Quaternion.identity,null);

    }
    IEnumerator spawnmanager(){
        waves++;
        for(int i=0;i<waves;i++){
            SpawnPulpo();
            yield return new WaitForSeconds(0.5f);
        }
    }
    void Update()
    {
        if (countDown<= 0f){
            StartCoroutine(spawnmanager());
            countDown = timeBetweenWaves;

        }
        countDown -= Time.deltaTime;
        countDown = Mathf.Clamp(countDown,0f,Mathf.Infinity);
    }
    
}
