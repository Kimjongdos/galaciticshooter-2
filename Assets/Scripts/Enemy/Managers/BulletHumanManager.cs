﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletHumanManager : MonoBehaviour
{
    private float time = 0;
    public float TimeToshoot;
    public GameObject BulletHuman;
    public AudioSource ShootSound;

    // Update is called once per frame
    void awake(){
        TimeToshoot= Random.Range(1,7);
        
    }
    
    void Update()
    {
        
        time+=Time.deltaTime;
        if(time>TimeToshoot){
        Instantiate(BulletHuman,transform.position,Quaternion.Euler(0,0,180),null);
        ShootSound.Play();
        time = 0;
        }
    }
}
