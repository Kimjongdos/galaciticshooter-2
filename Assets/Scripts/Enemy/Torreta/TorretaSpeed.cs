﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorretaSpeed : MonoBehaviour
{

    public float enemySpeed;
    public GameObject Bullet_up;
    public GameObject Bullet_Down;
    public GameObject Bullet_Left;
    public GameObject Bullet_Right;
     //Time Manager
    public float time;
    private float timecounter;
    

    

    void Update(){
        
        transform.Translate(Vector3.down*enemySpeed*Time.deltaTime,0);
        

        timecounter+= Time.deltaTime;
        if(timecounter>Random.Range(3,6)){
            Instantiate(Bullet_up,transform.position+new Vector3(-0.013f,0.603f,0),Quaternion.identity,null);
            Instantiate(Bullet_Down,transform.position+new Vector3(-0.013f,-0.573f,0),Quaternion.Euler(0,0,180),null);
            Instantiate(Bullet_Left,transform.position+new Vector3(-0.613f,0,0),Quaternion.Euler(0,0,90),null);
            Instantiate(Bullet_Right,transform.position+new Vector3(0.613f,0,0),Quaternion.Euler(0,0,-90),null);
            timecounter=0;
        }
    }
    
}

    
   

   