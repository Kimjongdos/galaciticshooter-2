﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Torreta_Bullets : MonoBehaviour
{
    public GameObject Bullet_up;
    
    //Time Manager
    public float time;
    private float timecounter;

    void Start(){
        
    }
    void Update()
    {
        timecounter+= Time.deltaTime;
        if(timecounter>time){
            Instantiate(Bullet_up,transform.position+new Vector3(0.02f,0.07f,0),Quaternion.Euler(0,0,transform.position.z),null);
            timecounter=0;
        }
    }
}
