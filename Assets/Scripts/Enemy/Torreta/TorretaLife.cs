﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TorretaLife : MonoBehaviour
{
    
    
    public float MaxHealthTorretaLife = 100f;
    public float currenthealth;
    public Collider2D kolaider;
    private Animator animDead;
    public AudioSource ExplosionSound;
    public Image HealthBar;
    public GameObject score100;
    private ScoreManager score;

    public GameObject _vida;
    
    
    //Animator animDead;

    void Start(){
    score =GameObject.FindGameObjectWithTag("ScoreManager").GetComponent<ScoreManager>();
        kolaider= GetComponent<Collider2D>();
        animDead = GetComponent<Animator>();
        currenthealth = MaxHealthTorretaLife;
        HealthBar.fillAmount = currenthealth/MaxHealthTorretaLife;
        
    }

    
    

   
    public void OnTriggerEnter2D(Collider2D other) {
       
        if(other.tag =="Bullet"){
               currenthealth -= other.GetComponent<SpeedBullet>().Damage;
               HealthBar.fillAmount = currenthealth/MaxHealthTorretaLife;
               if(currenthealth<=0){
               
                StartCoroutine(Destroi());
                  
               }
                  
                  

        }else if(other.tag=="Finish"){
            Destroy(this.gameObject);
        }

        IEnumerator Destroi(){
             Instantiate(score100,transform.position,Quaternion.identity,null);  
             score.AddScore(100);
            ExplosionSound.Play();
             kolaider.enabled = false;
            animDead.SetBool("muerte",true);
            _vida.SetActive(false);
            yield return new WaitForSeconds(1f);
            Destroy(this.gameObject);
        }
             
      
    }
}