﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumanSpeed : MonoBehaviour
{
    private ScoreManager score;
    public GameObject score50;
    private int speed;
    public AudioSource SoundDead;
    void Awake()
    {
        score =GameObject.FindGameObjectWithTag("ScoreManager").GetComponent<ScoreManager>();

    }
    void Update()
    {   
    transform.Translate(Vector3.down*Random.Range(0,6)*Time.deltaTime);  
    }    
    void OnTriggerEnter2D(Collider2D other)
    {
        if( other.tag=="Finish"||other.tag=="ShieldPower"){
        
         Destroy(gameObject);
        }else if(other.tag =="Bullet"){
            StartCoroutine(destroy());
        }

        IEnumerator destroy(){
            SoundDead.Play();
           Instantiate(score50,transform.position,Quaternion.identity,null);
           score.AddScore(50);
           yield return new WaitForSeconds(0.1f);
           Destroy(this.gameObject);
        }
    }
    
    
}
