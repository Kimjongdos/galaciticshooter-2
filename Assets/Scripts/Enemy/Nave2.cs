﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Nave2 : MonoBehaviour
{
   public GameObject Explosion;
   
   public Collider2D coll;
   private ScoreManager score;
   public GameObject score50;
   public AudioSource ExplosionBum;
   
   
   void Awake(){

    score =GameObject.FindGameObjectWithTag("ScoreManager").GetComponent<ScoreManager>();
    coll = GetComponent<Collider2D>();       
    }
   
   
   
    public void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag=="Bullet"){
            
                      
            StartCoroutine(DestroyNave2());
            


        }else if(other.tag=="Finish2"){
           Destroy(this.gameObject); 
        }
    }

    IEnumerator DestroyNave2(){
        // Add 50 
        ExplosionBum.Play();  
        score.AddScore(50);

        //Disable Graphics
        gameObject.GetComponent<Renderer>().enabled = false;
        //Instantiate Explosion
       
        Instantiate(Explosion,transform.position,Quaternion.identity,null);  
        Instantiate(score50,transform.position,Quaternion.identity,null);            
        yield return new WaitForSeconds(0.01f);
        Destroy(this.gameObject);
        
        
        
    }
}
