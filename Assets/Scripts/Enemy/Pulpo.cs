﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pulpo : MonoBehaviour
{
    // Start is called before the first frame update
    public float speed;
    public float speedx;
    public float MaxHealthPulpo = 100f;
    public float currenthealth;
    public Collider2D kolaider;
    private Animator animDead;
    public GameObject Score100;

    public Image HealthBar;

    public GameObject _vida;
    private ScoreManager score;
    
    
    
    //Animator animDead;

    void Awake(){
        kolaider= GetComponent<Collider2D>();
       animDead = GetComponent<Animator>();
        currenthealth = MaxHealthPulpo;
        HealthBar.fillAmount = currenthealth/MaxHealthPulpo;
        score =GameObject.FindGameObjectWithTag("ScoreManager").GetComponent<ScoreManager>();

        
    }
    
    

    // Update is called once per frame
    void Update()
    {
       
        transform.Translate(0,-speed*Time.deltaTime,0);
        
    }

   
    public void OnTriggerEnter2D(Collider2D other) {
       
        if(other.tag =="Bullet"){
               
               currenthealth -= other.GetComponent<SpeedBullet>().Damage;
               HealthBar.fillAmount = currenthealth/MaxHealthPulpo;
               if(currenthealth<=0){
                // Add 100 
                score.AddScore(100);
                Instantiate(Score100,transform.position,Quaternion.identity,null);
                kolaider.enabled = false;
                animDead.SetBool("Dead",true);
                _vida.SetActive(false);
                Destroy(this.gameObject);
                  
               }
                  
                  

        }else if(other.tag=="Finish"){
            Destroy(this.gameObject);
        }
             
      
    }
}