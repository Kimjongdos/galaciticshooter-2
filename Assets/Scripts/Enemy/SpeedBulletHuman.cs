﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedBulletHuman : MonoBehaviour
{
    public float speed;
    public float Damage= 10f;

    // Update is called once per frame
    void Update()
    {
        transform.Translate(0,speed*Time.deltaTime,0);
    }
    private void OnTriggerEnter2D(Collider2D other) {
       if(other.tag == "Finish"||other.tag =="Enemy")
        Destroy(this.gameObject);
    }
}
