﻿    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.UI;
    using UnityEngine.SceneManagement;

    public class Player : MonoBehaviour
    {
    //Life Player

    
    [Header("Limites")]
    private Vector2 axis;
    public Vector2 limits;
   
    [Header("Cadencias")]
    [SerializeField] float Timeto360Shoot;
    [SerializeField] float TimetoTripleShoot;
    [SerializeField] float TimetoShoot;
    [SerializeField] float currentTime;
    [Header("Audios")]
    [SerializeField] AudioSource SoundShoot;
    [SerializeField] AudioSource SoundDead;
    [SerializeField] AudioSource SoundHealth;
    [SerializeField] AudioSource SoundShield;
    [SerializeField] AudioSource SoundPowerUp;
    
    [Header("Velocidad")]
    [SerializeField] float speed;
    
    [Header("Vida Player")]
    [SerializeField] float MaxHealthPlayer = 100f;
    [SerializeField]float currenthealth;
    [SerializeField] Image HealthBar;
    [SerializeField] GameObject Shield;
    [SerializeField]public int Lives = 3;
    private Animator anim;
    
    [Header("Disparos")]
    public Weapon weapon;
    [SerializeField] bool CanTripleShoot = false;
    [SerializeField] bool Can360Shoot = false;
    [Header("Camara")]
    public CameraRotate camara;
    [SerializeField] GameObject sprite;
    [SerializeField] Collider2D coll;
    [Header("UI")]
    [SerializeField] GameObject ui_tripleshoot_a;
    [SerializeField] GameObject ui_shield_a;
    [SerializeField] GameObject ui_360shoot_a;
    [SerializeField] GameObject ui_tripleshoot;
    [SerializeField] GameObject ui_shield;
    [SerializeField] GameObject ui_360shoot;
    [Header("Score Script")]
    public ScoreManager Score;
    private int puntos;
   
    
   
    
    [Header("Propellers")]

    public Propellers prop;

    
    
    
    void Awake(){

    Cursor.visible = false; 
    //Alpha Active
    ui_360shoot_a.SetActive(true);
    ui_shield_a.SetActive(true);
    ui_tripleshoot_a.SetActive(true);
    ui_360shoot.SetActive(false);
    ui_shield.SetActive(false);
    ui_tripleshoot.SetActive(false);
    
    //Shield
    Shield.SetActive(false);
    //Cadencia CurrentTime
    currentTime = 0;
    //Components
    anim = GetComponent<Animator>();
    coll= GetComponent<Collider2D>();
    
    //Life of the player
    currenthealth = MaxHealthPlayer;
    HealthBar.fillAmount = currenthealth/MaxHealthPlayer;
   
        
    }


    void Update()
    {
   
    currentTime += Time.deltaTime;
  

    
    limitss();
    Movement();
    //Cadencia
    if(currentTime>TimetoShoot){
        InstantiateBullet(); 
        InstantiateTripleBullet(); 
        currentTime =0;
    }

    if (Input.GetKey(KeyCode.UpArrow))
    {
            prop.BlueFire();
        }else{
            prop.Stop();
        }

    }

    public void Movement()
    {
        //Up
        if (Input.GetKey(KeyCode.UpArrow)){
            transform.Translate(0,speed*Time.deltaTime,0);
            anim.SetBool("Right",false);
            anim.SetBool("Left",false);
        //Down
        }else if (Input.GetKey(KeyCode.DownArrow)){
            transform.Translate(0,-speed*Time.deltaTime,0);
        //Left       
        }else if (Input.GetKey(KeyCode.LeftArrow)){
        transform.Translate(-speed*Time.deltaTime,0,0);
        anim.SetBool("Left",true);
        anim.SetBool("Right",false);
        //Right
        }else if (Input.GetKey(KeyCode.RightArrow)){
            transform.Translate(speed*Time.deltaTime,0,0);
            anim.SetBool("Left",false);
            anim.SetBool("Right",true);
        }

    }
    public void limitss(){
        if (transform.position.x > limits.x) {
            transform.position = new Vector3 (limits.x, transform.position.y, transform.position.z);
        }else if (transform.position.x < -limits.x) {
            transform.position = new Vector3 (-limits.x, transform.position.y, transform.position.z);
        }
 
        if (transform.position.y > limits.y) {
            transform.position = new Vector3 (transform.position.x, limits.y, transform.position.z);
        }else if (transform.position.y < -limits.y) {
            transform.position = new Vector3 (transform.position.x, -limits.y, transform.position.z);
        }
    }
    public void InstantiateBullet(){
        
        if (Input.GetKeyDown("space")){
           if(CanTripleShoot ==false)
            weapon.Shoot();
            SoundShoot.Play();
            
            }
    }

     //Triple Shoot
     public void InstantiateTripleBullet(){
        
        if (Input.GetKeyDown("space")){
            if(CanTripleShoot == true){
                StartCoroutine(SecondsTripleShoot());
                weapon.TripleShoot();
                SoundShoot.Play();
                
            }else if(Can360Shoot == true){
                StartCoroutine(Seconds360Shoot());
                weapon.gradosshoot();
                SoundShoot.Play();
            }
        }
     }  
    

    //Collider 2D
    public void OnTriggerEnter2D(Collider2D other) {

        if(other.tag== "Health"){
            currenthealth=100;
            HealthBar.fillAmount = 1;
            SoundHealth.Play();
        }else if(other.tag == "Enemy"||other.tag == "BulletEnemy"||other.tag=="Meteor"){
                
            SoundDead.Play();
            LifePlayer();
            StartCoroutine(destroy());
            
        }else if(other.tag== "ShieldPower"){
            StartCoroutine(SHIELD());
        }else if(other.tag=="TripleShoot"){
            SoundPowerUp.Play();
            CanTripleShoot=true;
        }else if(other.tag=="Camera"){
            camara. rotatecam= true;
        }else if(other.tag =="360Shoot"){
            SoundPowerUp.Play();
            Can360Shoot = true;
        }
    }
   
        

    IEnumerator destroy(){
        
        
        //Lifes -1
        Lives--;
        if(Lives <= 0){
            StartCoroutine(Matame());
        }
        //Repeat 2 times
        for(int i=0;i<2;i++){
        //Disable Collider2D
        coll.enabled = false;
             
        sprite.SetActive(false);
        yield return new WaitForSeconds(0.2f);
        sprite.SetActive(true);
        yield return new WaitForSeconds(0.2f);
            sprite.SetActive(false);
            yield return new WaitForSeconds(0.2f);
            sprite.SetActive(true);
        }
        //Enable Collider
        coll.enabled = true;
        
        IEnumerator Matame(){
        puntos = Score.Scoreint;
        PlayerPrefs.SetInt("puntoss",puntos);

        //Ponemos la gravedad del rigidbody a 1
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        rb.gravityScale = 1.0f;

        yield return new WaitForSeconds(1.2f);

        SceneManager.LoadScene("GameOverScreen");
    }


    }
    IEnumerator SHIELD(){
        //Disable Collider2D
        coll.enabled = false;
        //Audio
        SoundShield.Play();
        //Shield Active
        Shield.SetActive(true);
        //UI Active
        ui_shield.SetActive(true);

        yield return new WaitForSeconds(10f);
        ui_shield.SetActive(false);
        Shield.SetActive(false);
        //Enable Collider
        coll.enabled = true;
    }

    IEnumerator Seconds360Shoot(){
        ui_360shoot.SetActive(true);
        yield return new WaitForSeconds(Timeto360Shoot);
        Can360Shoot = false;
        ui_360shoot.SetActive(false);
    }
    IEnumerator SecondsTripleShoot(){
        ui_tripleshoot.SetActive(true);
        yield return new WaitForSeconds(Timeto360Shoot);
        CanTripleShoot = false;
        ui_tripleshoot.SetActive(false);
    }

    
    

    public void LifePlayer(){
        
        currenthealth-=33.3f;
        
        HealthBar.fillAmount = currenthealth/MaxHealthPlayer; 
        
    }
    
}
