﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackHoleManager : MonoBehaviour
{
   public float Spawn;
   public GameObject BlackHole;
   public float timeBetweenWaves = 5f;
   private float countDown = 2f;
   private int waves = 0;

    // Update is called once per frame
    
    void SpawnBlackHole(){
    Instantiate(BlackHole,transform.position =new Vector3(Random.Range(-7.8f,7.8f),7,0),Quaternion.identity,null);

    }
    IEnumerator spawnmanager(){
        waves++;
        for(int i=0;i<waves;i++){
            SpawnBlackHole();
            yield return new WaitForSeconds(0.5f);
        }
    }
    void Update()
    {
        if (countDown<= 0f){
            StartCoroutine(spawnmanager());
            countDown = timeBetweenWaves;

        }
        countDown -= Time.deltaTime;
        countDown = Mathf.Clamp(countDown,0f,Mathf.Infinity);
    }
    
}
