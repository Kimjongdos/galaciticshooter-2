﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedBlackHole : MonoBehaviour
{
    public int speed;
    public GameObject meteorPrefab;

    public float timeLauchMeteor;

    private float currentTime = 0;

    void Update()
    {
        transform.Translate(Vector3.down*Time.deltaTime);
         currentTime += Time.deltaTime;
        
        if (currentTime>timeLauchMeteor){
            currentTime = 0;
            meteor();
        }
    }
    private void OnTriggerEnter2D(Collider2D other) {
       if(other.tag == "Finish"||other.tag =="Player")Destroy(this.gameObject);
    }
    public  void meteor(){
        Instantiate(meteorPrefab,transform.position, Quaternion.Euler(0,0,Random.Range(0,360)), null);
        
    }
}
