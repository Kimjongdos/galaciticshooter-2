﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
   public int speed;
   private Rigidbody2D rbd;


   void Start(){
       speed = 5;
       //rbd = GetComponent<Rigidbody2D>();
   }

   void Update(){
       transform.Translate(0,-speed*Time.deltaTime,0);
   }

   
   /*void FixedUpdate(){
       rbd.velocity = new Vector3(0,speed*Time.deltaTime,0);

   }*/
   
   
    public void OnTriggerEnter2D(Collider2D other) {

        if(other.tag== "Player"){
            Destroy(this.gameObject);
        }else if(other.tag=="Finish2"){
            Destroy(this.gameObject);
        }
    }
    
}
