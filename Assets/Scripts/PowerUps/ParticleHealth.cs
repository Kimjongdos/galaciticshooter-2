﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleHealth : MonoBehaviour
{
    public GameObject particle;

    // Update is called once per frame
    private void OnTriggerEnter2D(Collider2D other) {
        if(other.tag=="Player"){
            Instantiate(particle,transform.position,Quaternion.identity,null);
        }else{
            Destroy(gameObject);
        }
        
    }
}
