﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotate : MonoBehaviour
{
    public float SpeedRotate;
    
    
    float smooth = 3;
    public float tiltAngle = 180.0f;
    public bool rotatecam;
    public float TimeCamera;
    public GameObject RotateSprite;


    void Start(){
       
    }
    void Update()
    {
        if(rotatecam== true){
            
            StartCoroutine(RoteitCamera());
        }
    }

    IEnumerator RoteitCamera(){
        RotateSprite.SetActive(true);  
        Instantiate(RotateSprite,transform.position,Quaternion.identity,null);
        // Rotate the cube by converting the angles into a quaternion.
        Quaternion target = Quaternion.Euler(0, 0, -1* tiltAngle);
        Quaternion target0 = Quaternion.Euler(0, 0, 0);
        // Dampen towards the target rotation
        
        transform.rotation = Quaternion.Slerp(transform.rotation, target,  Time.deltaTime * smooth);
       RotateSprite.SetActive(false);
        yield return new WaitForSeconds(TimeCamera);
        transform.rotation =Quaternion.Euler(0,0,0);
        
        rotatecam = false;

    }
}
