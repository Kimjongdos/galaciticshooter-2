﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelChanger : MonoBehaviour
{

public Animator animator;

private int screenToLoad;

private float currentLogoTime=0;

public float logoTime;

    void Update()
    {

        currentLogoTime+=Time.deltaTime;

        if(currentLogoTime>=logoTime)
        {
            FadeToLevel(1);
        }

        else if(Input.anyKey)
        {
            FadeToLevel(1);
        }
    }

public void FadeToLevel (int levelIndex)
{
    screenToLoad = levelIndex;
    animator.SetTrigger("FadeOut");
}

public void OnFadeComplete ()
{
    SceneManager.LoadScene(screenToLoad);
}

}
