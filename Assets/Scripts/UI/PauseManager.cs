﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
 
public class PauseManager : MonoBehaviour
{
    private bool isPaused = false;
    [SerializeField] GameObject PauseCanvas;
    [SerializeField] GameObject CanvasUI;
    [SerializeField] Text score;
    [Header("UI")]
    [SerializeField] GameObject ui_tripleshoot_a;
    [SerializeField] GameObject ui_shield_a;
    [SerializeField] GameObject ui_360shoot_a;
    [SerializeField] GameObject ui_tripleshoot;
    [SerializeField] GameObject ui_shield;
    [SerializeField] GameObject ui_360shoot;
 
 
    
    public void Continue(){
        Cursor.visible = false;
        PauseCanvas.SetActive(false);
        CanvasUI.SetActive(true);
        uiPowerupstrue();
        Time.timeScale = 1.0f;
        isPaused = false;
    }
 
    public void Quit(){
        Time.timeScale = 1.0f;
        SceneManager.LoadScene("TitleScreen");
    }
 
 
    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
        if (!isPaused && Input.GetKeyDown(KeyCode.Escape))
        {
            ActivatePause();
        }else if(isPaused && Input.GetKeyDown(KeyCode.Escape)){
            Continue();
        }
    }
 
    void ActivatePause(){
        Cursor.visible = true;
        isPaused = true;
        score.text = PlayerPrefs.GetInt("puntos").ToString();
        PauseCanvas.SetActive(true);
        CanvasUI.SetActive(false);
        uiPowerupsfalse();
 
        Time.timeScale = 0;
    }
    void uiPowerupsfalse(){
    ui_360shoot.SetActive(false);
    ui_shield.SetActive(false);
    ui_tripleshoot.SetActive(false);
    ui_360shoot_a.SetActive(false);
    ui_shield_a.SetActive(false);
    ui_tripleshoot_a.SetActive(false);
    }
    void uiPowerupstrue(){
    ui_360shoot.SetActive(true);
    ui_shield.SetActive(true);
    ui_tripleshoot.SetActive(true);
    ui_360shoot_a.SetActive(true);
    ui_shield_a.SetActive(true);
    ui_tripleshoot_a.SetActive(true);
    }

}