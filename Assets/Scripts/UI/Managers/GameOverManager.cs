﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOverManager : MonoBehaviour
{

    
    public void PulsaRetry(){

            Debug.LogError("He pulsado Retry");    

            SceneManager.LoadScene("GamePlayScreen");
    }
    
    public void PulsaQuit(){

        SceneManager.LoadScene("TitleScreen");
    }

    

}
