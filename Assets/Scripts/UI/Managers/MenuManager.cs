﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour


{
public AudioSource click;

    public void PulsaStart(){

          

            click.Play(); 

            SceneManager.LoadScene("GamePlayScreen");
    }

        public void PulsaCredits(){

            

            click.Play(); 

            SceneManager.LoadScene("CreditsScreen");
    }

    public void PulsaControls(){

         

            click.Play(); 

            SceneManager.LoadScene("ControlsScreen");
    }
    
    public void PulsaExit(){

        Application.Quit();
    }

}
