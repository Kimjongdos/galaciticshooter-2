﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpritesCreditsMov : MonoBehaviour
{
    Vector2 speed;
    Transform graphics;


    public void Awake()
    {
        graphics = transform.GetChild(0);

        for(int i=0; i<graphics.childCount;i++){
            graphics.GetChild(i).gameObject.SetActive(false);
        }

        int seleccionado = Random.Range(0,graphics.childCount);
        graphics.GetChild(seleccionado).gameObject.SetActive(true);

        speed.x = Random.Range(-5,-2);
        speed.y = Random.Range(-3,3);
    }

    void Update(){
        transform.Translate(speed*Time.deltaTime);
        
    }

    public void OnTriggerEnter2D(Collider2D other){
        if (other.tag == "Finish"){
            Destroy(this.gameObject);
        } 
    }

 


}
